<?php
$pluginsurl = plugins_url( '', __FILE__ );

$wccp_pro_settings = wccp_pro_read_options();

global $wccp_pro_settings;

function wccp_pro_disable_Right_Click()
{
global $wccp_pro_settings;
?>
<script id="wccp_pro_disable_Right_Click" type="text/javascript">
	//<![CDATA[
	    function nocontext(e) {
			
			e = e || window.event; // also there is no e.target property in IE. instead IE uses window.event.srcElement
			console.log('RC '+e);
			if (apply_class_exclusion(e) == 'Yes') return true;
			
	    	var exception_tags = 'NOTAG,';
	        var clickedTag = (e==null) ? event.srcElement.tagName : e.target.tagName;
	        //alert(clickedTag);
	        var checker = '<?php echo $wccp_pro_settings['img'];?>';
	        if ((clickedTag == "IMG" || clickedTag == "PROTECTEDIMGDIV") && checker == 'checked') {
	            if (alertMsg_IMG != "")show_wccp_pro_message(alertMsg_IMG);
	            return false;
	        }else {exception_tags = exception_tags + 'IMG,';}
			
			checker = '<?php echo $wccp_pro_settings['videos'];?>';
			if ((clickedTag == "VIDEO" || clickedTag == "PROTECTEDWCCPVIDEO" || clickedTag == "EMBED") && checker == 'checked') {
	            if (alertMsg_VIDEO != "")show_wccp_pro_message(alertMsg_VIDEO);
	            return false;
	        }else {exception_tags = exception_tags + 'VIDEO,PROTECTEDWCCPVIDEO,EMBED,';}
	        
	        checker = '<?php echo $wccp_pro_settings['a'];?>';
	        if ((clickedTag == "A" || clickedTag == "TIME") && checker == 'checked') {
	            if (alertMsg_A != "")show_wccp_pro_message(alertMsg_A);
	            return false;
	        }else {exception_tags = exception_tags + 'A,';}
	        
	        checker = '<?php echo $wccp_pro_settings['pb'];?>';
	        if ((clickedTag == "P" || clickedTag == "B" || clickedTag == "FONT" ||  clickedTag == "LI" || clickedTag == "UL" || clickedTag == "STRONG" || clickedTag == "OL" || clickedTag == "BLOCKQUOTE" || clickedTag == "TD" || clickedTag == "SPAN" || clickedTag == "EM" || clickedTag == "SMALL" || clickedTag == "I" || clickedTag == "BUTTON") && checker == 'checked') {
	            if (alertMsg_PB != "")show_wccp_pro_message(alertMsg_PB);
	            return false;
	        }else {exception_tags = exception_tags + 'P,B,FONT,LI,UL,STRONG,OL,BLOCKQUOTE,TD,SPAN,EM,SMALL,I,BUTTON,';}
	        
	        checker = '<?php echo $wccp_pro_settings['input'];?>';
	        if ((clickedTag == "INPUT" || clickedTag == "PASSWORD") && checker == 'checked') {
	            if (alertMsg_INPUT != "")show_wccp_pro_message(alertMsg_INPUT);
	            return false;
	        }else {exception_tags = exception_tags + 'INPUT,PASSWORD,';}
	        
	        checker = '<?php echo $wccp_pro_settings['h'];?>';
	        if ((clickedTag == "H1" || clickedTag == "H2" || clickedTag == "H3" || clickedTag == "H4" || clickedTag == "H5" || clickedTag == "H6" || clickedTag == "ASIDE" || clickedTag == "NAV") && checker == 'checked') {
	            if (alertMsg_H != "")show_wccp_pro_message(alertMsg_H);
	            return false;
	        }else {exception_tags = exception_tags + 'H1,H2,H3,H4,H5,H6,';}
	        
	        checker = '<?php echo $wccp_pro_settings['textarea'];?>';
	        if (clickedTag == "TEXTAREA" && checker == 'checked') {
	            if (alertMsg_TEXTAREA != "")show_wccp_pro_message(alertMsg_TEXTAREA);
	            return false;
	        }else {exception_tags = exception_tags + 'TEXTAREA,';}
	        
	        checker = '<?php echo $wccp_pro_settings['emptyspaces'];?>';
	        if ((clickedTag == "DIV" || clickedTag == "BODY" || clickedTag == "HTML" || clickedTag == "ARTICLE" || clickedTag == "SECTION" || clickedTag == "NAV" || clickedTag == "HEADER" || clickedTag == "FOOTER") && checker == 'checked') {
	            if (alertMsg_EmptySpaces != "")show_wccp_pro_message(alertMsg_EmptySpaces);
	            return false;
	        }
	        else
	        {
				//show_wccp_pro_message(exception_tags.indexOf(clickedTag));
	        	if (exception_tags.indexOf(clickedTag)!=-1)
	        	{
		        	return true;
		        }
	        	else
	        	return false;
	        }
	    }
	    var alertMsg_IMG = "<?php echo addslashes($wccp_pro_settings['alert_msg_img']);?>";
	    var alertMsg_A = "<?php echo addslashes($wccp_pro_settings['alert_msg_a']);?>";
	    var alertMsg_PB = "<?php echo addslashes($wccp_pro_settings['alert_msg_pb']);?>";
	    var alertMsg_INPUT = "<?php echo addslashes($wccp_pro_settings['alert_msg_input']);?>";
	    var alertMsg_H = "<?php echo addslashes($wccp_pro_settings['alert_msg_h']);?>";
	    var alertMsg_TEXTAREA = "<?php echo addslashes($wccp_pro_settings['alert_msg_textarea']);?>";
	    var alertMsg_EmptySpaces = "<?php echo addslashes($wccp_pro_settings['alert_msg_emptyspaces']);?>";
		var alertMsg_VIDEO = "<?php echo addslashes($wccp_pro_settings['alert_msg_videos']);?>";
	    document.oncontextmenu = nocontext;
		//document.body.oncontextmenu=function(event) { nocontext(); };
	//]]>
	</script>
<?php
}?>
<?php
///////////////////////////////////////////////////////////
function wccp_pro_disable_prntscr_key()
{
	global $wccp_pro_settings;
	?>
	<script type="text/javascript">
	jQuery(document).bind("keyup keydown", function(e){
		e = e || window.event; // also there is no e.target property in IE. instead IE uses window.event.srcElement
		console.log (e.keyCode);
		dealWithPrintScrKey(e);
});
	
	//window.addEventListener("keyup", dealWithPrintScrKey, false);
	document.onkeyup = dealWithPrintScrKey;
	function dealWithPrintScrKey(e)
	{
		e = e || window.event; // also there is no e.target property in IE. instead IE uses window.event.srcElement
		
		// gets called when any of the keyboard events are overheard
		var prtsc = e.keyCode||e.charCode;
		
		//alert (prtsc);

		if (prtsc == 44)
		{
			e.cancelBubble = true;
			e.preventDefault();
			e.stopImmediatePropagation();
			show_wccp_pro_message('<?php echo addslashes($wccp_pro_settings['custom_keys_message']);?>');
		}
	}
	</script>
	<?php if($wccp_pro_settings['prnt_scr_msg'] != ''){ ?>
	<style>
	@media print {
	body * { display: none !important;}
		body:after {
		content: "<?php echo $wccp_pro_settings['prnt_scr_msg']; ?>"; }
	}
	</style>
	<?php } ?>
<?php
}
///////////////////////////////////////////////////////////
function wccp_pro_disable_selection_footer()
{
	global $wccp_pro_settings;
	echo '<style type="text/css" data-asas-style="">body, div, p, span{ cursor: inherit ; user-select: none !important; }
	a{ cursor: pointer ; user-select: none !important; }
	h1, h2, h3, h4, h5, h6{ cursor: default ; user-select: none !important; }
	</style>';
	$selection_exclude_classes = get_selection_exclude_classes();
	
	$selection_exclude_classes2 = str_replace(",", ", .", $selection_exclude_classes);
	
	echo "<style> ." . $selection_exclude_classes2 . " " . "{cursor: text !important; user-select: text !important;}</style>";
	
	$contenteditable_inputs = "TEXT,TEXTAREA,input[type=text],input[type=password],input[type=number]";
	
	echo "<style>" . $contenteditable_inputs . " " . "{cursor: text !important; user-select: text !important;}</style>";
	
	$contenteditable_inputs_selection = str_replace(",", "::selection, ", $contenteditable_inputs);
	
	//echo "<style>" . $contenteditable_inputs_selection . "::selection{background-color: #338FFF !important; color: #fff !important;}</style>";
	
	$selection_exclude_classes3 = str_replace(",", "::selection, .", $selection_exclude_classes);
	
	echo "<style> ." . $selection_exclude_classes3 . "::selection{background-color: #338FFF !important; color: #fff !important;}</style>";
	
	//Loop here to create a full string of selection_exclude_classes2 with all tags seperated by commas
	$tags_array = array("body" , "div" , "p" , "span" , "h1" , "h2" , "h3" , "h4" , "h5", "a");
	
	foreach($tags_array as $tag_name){
		
		$selection_exclude_classes2 = str_replace(",", " > $tag_name ,.", $selection_exclude_classes);
	
		echo "<style> ." . $selection_exclude_classes2 . " > $tag_name" . "{cursor: text !important; user-select: text !important;}</style>";
		
		$selection_exclude_classes3 = str_replace(",", " $tag_name::selection, .", $selection_exclude_classes);
		
		echo "<style> ." . $selection_exclude_classes3 . " $tag_name::selection{background-color: #338FFF !important; color: #fff !important;}</style>";
	}
}
///////////////////////////////////////////////////////////
function wccp_pro_disable_hot_keys()
{
global $wccp_pro_settings;
?>
<script id="wccp_pro_disable_hot_keys" type="text/javascript">
//<![CDATA[
//////////////////Hot keys function
function disable_hot_keys(e)
{
	var key_number;
	
		if(window.event)
			  key_number = window.event.keyCode;     //IE
		else
			key_number = e.which;     //firefox (97)

	/////////////////////////////////////////////Case F12
	<?php if($wccp_pro_settings['f12_protection'] == 'checked')
	{ ?>
		if (key_number == 123)//F12 chrome developer key disable
		{
			show_wccp_pro_message('<?php echo addslashes($wccp_pro_settings['custom_keys_message']);?>');
			return false;
		}
	<?php } ?>
	var elemtype = e.target.tagName;
	
	elemtype = elemtype.toUpperCase();
	
	if (elemtype == "TEXT" || elemtype == "TEXTAREA" || elemtype == "INPUT" || elemtype == "PASSWORD" || elemtype == "SELECT")
	{
		elemtype = 'TEXT';
	}
	
	if(wccp_pro_iscontenteditable(e)) elemtype = 'TEXT';
	
	if (e.ctrlKey || e.metaKey)
	{
		var key = key_number;
		
		console.log(key);

		if (elemtype!= 'TEXT' && (key == 97 || key == 99 || key == 120 || key == 26 || key == 43))
		{
			 show_wccp_pro_message('<?php echo addslashes($wccp_pro_settings['ctrl_message']);?>');
			 return false;
		}
		if (elemtype!= 'TEXT')
		{
			/////////////////////////////////////////////Case Ctrl + A 65
			<?php if($wccp_pro_settings['ctrl_a_protection'] == 'checked')
			{ ?>
			
			if (key == 65)
			{
				show_wccp_pro_message('<?php echo addslashes($wccp_pro_settings['custom_keys_message']);?>');
				return false;
			}<?php } ?>
			
			/////////////////////////////////////////////Case Ctrl + C 67
			<?php if($wccp_pro_settings['ctrl_c_protection'] == 'checked')
			{ ?>
			
			if (key == 67)
			{
				show_wccp_pro_message('<?php echo addslashes($wccp_pro_settings['custom_keys_message']);?>');
				return false;
			}<?php } ?>
			
			/////////////////////////////////////////////Case Ctrl + X 88
			<?php if($wccp_pro_settings['ctrl_x_protection'] == 'checked')
			{ ?>
			
			if (key == 88)
			{
				show_wccp_pro_message('<?php echo addslashes($wccp_pro_settings['custom_keys_message']);?>');
				return false;
			}<?php } ?>
			
			/////////////////////////////////////////////Case Ctrl + V 86
			<?php if($wccp_pro_settings['ctrl_v_protection'] == 'checked')
			{ ?>
			
			if (key == 86)
			{
				show_wccp_pro_message('<?php echo addslashes($wccp_pro_settings['custom_keys_message']);?>');
				return false;
			}<?php } ?>
			
			/////////////////////////////////////////////Case Ctrl + U 85
			<?php if($wccp_pro_settings['ctrl_u_protection'] == 'checked')
			{ ?>
			
			if (key == 85)
			{
				show_wccp_pro_message('<?php echo addslashes($wccp_pro_settings['custom_keys_message']);?>');
				return false;
			}<?php } ?>
		}
		
		//For any emement type, text elemtype is not excluded here
		/////////////////////////////////////////////Case Ctrl + P 80 (prntscr = 44)
		<?php if($wccp_pro_settings['ctrl_p_protection'] == 'checked')
		{ ?>
		if (key == 80 || key_number == 44)
		{
			show_wccp_pro_message('<?php echo addslashes($wccp_pro_settings['custom_keys_message']);?>');
			return false;
		}<?php } ?>
		
		/////////////////////////////////////////////Case Ctrl + S 83
		<?php if($wccp_pro_settings['ctrl_s_protection'] == 'checked')
		{ ?>
		
		if (key == 83)
		{
			show_wccp_pro_message('<?php echo addslashes($wccp_pro_settings['custom_keys_message']);?>');
			return false;
		}<?php } ?>
    }
return true;
}
//document.oncopy = function(){return false;};
jQuery(document).bind("keyup keydown", disable_hot_keys);
</script>
<?php
}
///////////////////////////////////////////////////////////
function wccp_pro_disable_selection()
{
global $wccp_pro_settings;
?>
<script id="wccp_pro_disable_selection" type="text/javascript">
//<![CDATA[

var image_save_msg='You can\'t save images!';
var no_menu_msg='Context menu disabled!';
var smessage = "<?php echo $wccp_pro_settings['smessage'];?>";
	
document.addEventListener('allow_copy', e => {
    if (e.detail) {
        // Stop extension functionality
		const event = new CustomEvent('allow_copy', { detail: { unlock: false } })
		window.top.document.dispatchEvent(event)
    }
});

"use strict";
// This because search property "includes" does not supported by IE
if (!String.prototype.includes) {
String.prototype.includes = function(search, start) {
  if (typeof start !== 'number') {
	start = 0;
  }

  if (start + search.length > this.length) {
	return false;
  } else {
	return this.indexOf(search, start) !== -1;
  }
};
}
////////////////For contenteditable tags
function wccp_pro_iscontenteditable(e)
{
	var e = e || window.event; // also there is no e.target property in IE. instead IE uses window.event.srcElement
  	
	var target = e.target || e.srcElement;

	var elemtype = e.target.nodeName;
	
	elemtype = elemtype.toUpperCase();
	
	var iscontenteditable = "false";
		
	if(typeof target.getAttribute!="undefined" ) iscontenteditable = target.getAttribute("contenteditable"); // Return true or false as string
	
	var iscontenteditable2 = false;
	
	if(typeof target.isContentEditable!="undefined" ) iscontenteditable2 = target.isContentEditable; // Return true or false as boolean

	if(target.parentElement.isContentEditable) iscontenteditable2 = true;
	
	if (iscontenteditable == "true" || iscontenteditable2 == true)
	{
		if(typeof target.style!="undefined" ) target.style.cursor = "text";
		
		return true;
	}
}
////////////////
function disable_copy(e)
{
	var e = e || window.event; // also there is no e.target property in IE. instead IE uses window.event.srcElement
  	
	var target = e.target || e.srcElement;

	var elemtype = e.target.nodeName;
	
	elemtype = elemtype.toUpperCase();
	
	if(wccp_pro_iscontenteditable(e)) return true;
	
	if (apply_class_exclusion(e) == "Yes") return true;
	
	//disable context menu when shift + right click is pressed
	var shiftPressed = 0;
	
	var evt = e?e:window.event;
	
	if (parseInt(navigator.appVersion)>3) {
		
		if (document.layers && navigator.appName=="Netscape")
			
			shiftPressed = (e.modifiers-0>3);
			
		else
			
			shiftPressed = e.shiftKey;
			
		if (shiftPressed) {
			
			if (smessage !== "") show_wccp_pro_message(smessage);
			
			var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
			
			if (isFirefox) {
			evt.cancelBubble = true;
			if (evt.stopPropagation) evt.stopPropagation();
			if (evt.preventDefault()) evt.preventDefault();
			console.log(evt);
			show_wccp_pro_message (smessage);
			return false;
			}
			
			return false;
		}
	}
	
	if(e.which === 2 ){
	var clickedTag_a = (e==null) ? event.srcElement.tagName : e.target.tagName;
	   show_wccp_pro_message(smessage);
       return false;
    }
	var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
	var checker_IMG = '<?php echo $wccp_pro_settings['img'];?>';
	if (elemtype == "IMG" && checker_IMG == 'checked' && e.detail == 2) {show_wccp_pro_message(alertMsg_IMG);return false;}

    if (elemtype != "TEXT" && elemtype != "TEXTAREA" && elemtype != "INPUT" && elemtype != "PASSWORD" && elemtype != "SELECT" && elemtype != "OPTION" && elemtype != "EMBED")
	{
		if (smessage !== "" && e.detail == 2)
			show_wccp_pro_message(smessage);
		
		if (isSafari)
			return true;
		else
			return false;
	}	
}
function disable_copy_ie()
{
	var e = e || window.event;
  // also there is no e.target property in IE.
  // instead IE uses window.event.srcElement
  	var target = e.target || e.srcElement;
	
	var elemtype = window.event.srcElement.nodeName;
	
	elemtype = elemtype.toUpperCase();

	if(wccp_pro_iscontenteditable(e)) return true;
	
	if (apply_class_exclusion(e) == "Yes") return true;
	
	//var disable_drag_drop = '<?php echo $wccp_pro_settings['drag_drop'];?>';
	
	//if (elemtype == "IMG" & disable_drag_drop != "checked")  return true;
	
	if (elemtype == "IMG") {show_wccp_pro_message(alertMsg_IMG);return false;}
	
	if (elemtype != "TEXT" && elemtype != "TEXTAREA" && elemtype != "INPUT" && elemtype != "PASSWORD" && elemtype != "SELECT" && elemtype != "EMBED" && elemtype != "OPTION")	
	{
		return false;
	}
}	
function reEnable()
{
	return true;
}

//document.oncopy = function(){return false;};
//document.onkeydown = disable_hot_keys;
function disable_drag(e)
{
	//var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
	//if (isSafari) {show_wccp_pro_message(alertMsg_IMG);return false;}
	
	var e = e || window.event; // also there is no e.target property in IE. instead IE uses window.event.srcElement
  	
	var target = e.target || e.srcElement;
	
	//For contenteditable tags
	
	
	if (apply_class_exclusion(e) == "Yes") return true;

	var elemtype = e.target.nodeName;
	
	elemtype = elemtype.toUpperCase();
	
	//console.log(elemtype);
	
	var disable_drag_drop = '<?php echo $wccp_pro_settings['drag_drop'];?>';
	
	if (disable_drag_drop != "checked")  return true;
	
	return false;
}

if(navigator.userAgent.indexOf('MSIE')==-1) //If not IE
{
	document.ondragstart = disable_drag;
	document.onselectstart = disable_copy;
	document.onclick = reEnable;
}else
{
	document.onselectstart = disable_copy_ie;
}

//////////////////special for safari Start////////////////
var onlongtouch; 
var timer;
var touchduration = 1000; //length of time we want the user to touch before we do something

var elemtype = "";
function touchstart(e) {
	var e = e || window.event;
  // also there is no e.target property in IE.
  // instead IE uses window.event.srcElement
  	var target = e.target || e.srcElement;
	
	elemtype = window.event.srcElement.nodeName;
	
	elemtype = elemtype.toUpperCase();
	
	if(!wccp_pro_is_passive()) e.preventDefault();
	if (!timer) {
		timer = setTimeout(onlongtouch, touchduration);
	}
}

function touchend() {
    //stops short touches from firing the event
    if (timer) {
        clearTimeout(timer);
        timer = null;
    }
	onlongtouch();
}

onlongtouch = function(e) { //this will clear the current selection if anything selected
	
	if (elemtype != "TEXT" && elemtype != "TEXTAREA" && elemtype != "INPUT" && elemtype != "PASSWORD" && elemtype != "SELECT" && elemtype != "EMBED" && elemtype != "OPTION")	
	{
		if (window.getSelection) {
			if (window.getSelection().empty) {  // Chrome
			window.getSelection().empty();
			} else if (window.getSelection().removeAllRanges) {  // Firefox
			window.getSelection().removeAllRanges();
			}
		} else if (document.selection) {  // IE?
			document.selection.empty();
		}
		return false;
	}
};

document.addEventListener("DOMContentLoaded", function(event) { 
    window.addEventListener("touchstart", touchstart, false);
    window.addEventListener("touchend", touchend, false);
});

function wccp_pro_is_passive() {

  var cold = false,
  hike = function() {};

  try {
  var aid = Object.defineProperty({}, 'passive', {
  get() {cold = true}
  });
  window.addEventListener('test', hike, aid);
  window.removeEventListener('test', hike, aid);
  } catch (e) {}

  return cold;
}

function wccp_pro_is_mobile() {
	var isMobile = false; //initiate as false
	// device detection
	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
		|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
		isMobile = true;
	}
}
/*
//Here's the accepted answer, but in two lines of code:
var selection = window.getSelection ? window.getSelection() : document.selection ? document.selection : null;
if(!!selection) selection.empty ? selection.empty() : selection.removeAllRanges();
*/
/*
jQuery("a").on("mousedown", disable_copy);
jQuery("a").on("mouseup", disable_copy);
jQuery("a").on("touchstart", disable_copy);
jQuery("a").on("touchend", disable_copy);
jQuery("a").on("click", disable_copy);
*/
//jQuery(document.body).on("touchstart", disable_copy);
//jQuery(document.body).on("touchend", disable_copy);
//////////////////special for safari End////////////////
//]]>
</script>
<?php
}
///////////////////////////////////////////////////////////
function wccp_pro_images_overlay()
{
	global $pluginsurl,$wccp_pro_settings;
	?>
	<!--Smart protection techniques -->
	<script type="text/javascript">
		jQuery("img").wrap('<div style="position: relative;height: 100%;width: 100%;"></div>');
		jQuery("img").after('<ProtectedImgDiv class="protectionOverlaytext"><img src= "<?php echo $pluginsurl."/images/transparent.gif";?>" style="width:100%; height:100%" /><p><?php //echo $wccp_pro_settings['dw_text'];?></p></ProtectedImgDiv>');
	</script>
	<style>
	.protectionOverlaytext{
		position: absolute;
		width: 100%;
		height: 100%;
		top: 0;
		left: 0;
		display: block;
		z-index: 999;
		-webkit-touch-callout: none;
		-webkit-user-select: none;
		-khtml-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;

		font-weight: bold;
		opacity: 0.<?php echo $wccp_pro_settings['dw_text_transparency'];?>;
		text-align: center;
		transform: rotate(0deg);
	}
	.protectionOverlaytext img{
		-webkit-touch-callout: none;
		-webkit-user-select: none;
		-khtml-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		pointer-events: none;
	}
	</style>
	<!--Smart protection techniques -->
	<?php
}
?>
<?php
function wccp_pro_video_overlay()
{
	?>
	<!--just for video protection -->
	<style>
	.pointer_events_none{
		pointer-events: none;
	}
	.pointer_events_auto{
		pointer-events: auto;
	}
	</style>
	<script type="text/javascript">	
	/*
	jQuery(function() {
		
		jQuery('IFRAME').load(function(e) {
			iframe_load(this);
		});
		
		jQuery('IFRAME').live('mouseenter touchstart',function(e) {
			//iframe_load(this);
		});
	});*/

	function play_stop_video(ev)
	{
		jQuery('Protectedwccpvideo').addClass("pointer_events_none");
		setTimeout(function(){ jQuery('Protectedwccpvideo').removeClass("pointer_events_none"); }, 1000);
	}
	
	function play_stop_video_2(ev)
	{
		jQuery('Protectedwccpvideo').addClass("pointer_events_none");
		setTimeout(function(){ jQuery('Protectedwccpvideo').removeClass("pointer_events_none"); }, 100);
	}
	
	jQuery("iframe").wrap('<div style="position: relative;height: 100%;width: 100%;"></div>');
	jQuery("iframe").after('<Protectedwccpvideo onclick="play_stop_video(this)" class="protected_video_class"><div onmousemove="play_stop_video_2(this)" onclick="play_stop_video(this)" class="Protectedwccpvideomiddle_class"></div></Protectedwccpvideo>');

	</script>
	<style>
	.Protectedwccpvideomiddle_class {
	  background-color: #FF4136;
	  width: 70px;
	  height: 50px;
	  position: absolute;
	  left: 50%;
	  top: 50%;
	  transform: translate(-50%, -50%);
	}
	.protected_video_class{
		position: absolute;
		width: 100%;
		height: 100%;
		top: 0;
		left: 0;
		display: block;
		z-index: 999;
		border: 1px solid red;
		font-weight: bold;
		opacity: 0.0;
		text-align: center;
		transform: rotate(0deg);
	}
	</style>
	<style>
	wccpvideo {
		background: transparent none repeat scroll 0 0;
		border: 2px solid #fff;
		//opacity: 0.0;
	}
	.protectionOverlay{
		background: #fff none repeat scroll 0 0;
		border: 2px solid #fff;
		opacity: 0.0;
	}
	</style>
	<!--just for iphones end -->
	<?php
}
?>