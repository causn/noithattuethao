<?php
//---------------------------------------------------------------------------------------------
//Register libraries using new wordpress register_script & enqueue_script functions
//---------------------------------------------------------------------------------------------
$pluginsurl = plugins_url( '', __FILE__ );

$c = new wccp_pro_controls_class();

function wccp_pro_enqueue_scripts() {

	$pluginsurl = plugins_url( '', __FILE__ );
	
	$admincore = '';
	
	if (isset($_GET['page'])) $admincore = $_GET['page'];
	
	if( is_admin() && $admincore == 'wccp-options-pro') {
	
		wp_enqueue_script('jquery');
		
		wp_register_style('font-awesome.min', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
		wp_enqueue_style('font-awesome.min');
		
		wp_register_style('defaultcss', $pluginsurl.'/css/responsive-pure-css-tabs/default.css');
		wp_enqueue_style('defaultcss');
		
		wp_register_style('stylecss', $pluginsurl.'/css/responsive-pure-css-tabs/style.css?v=4');
		wp_enqueue_style('stylecss');
		
		wp_register_script('responsive_pure_css_tabsjs', $pluginsurl.'/css/responsive-pure-css-tabs/js.js');
		wp_enqueue_script('responsive_pure_css_tabsjs');
		
		if(is_rtl() == 'rtl')
			wp_register_style('bootstrapcss', $pluginsurl.'/bootstrap/css/bootstrap-rtl.min.css');
		else
			wp_register_style('bootstrapcss', $pluginsurl.'/bootstrap/css/bootstrap.min.css');
		wp_enqueue_style('bootstrapcss');
		
		wp_register_script('bootstrap-bundle-min-js', $pluginsurl.'/bootstrap/js/bootstrap.bundle.min.js');
		wp_enqueue_script('bootstrap-bundle-min-js');
		
		wp_enqueue_script( 'wccppro_slimselect', 'https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.25.0/slimselect.min.js','1.0.0',true );
		wp_register_style('wccppro_slimselect_css', 'https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.25.0/slimselect.min.css', false, '1.0.0' );
		wp_enqueue_style('wccppro_slimselect_css');
		
		wp_register_script('flat-ui.min', $pluginsurl.'/flat-ui/js/flat-ui.min.js');
		wp_enqueue_script('flat-ui.min');
		
		wp_register_script('applicationjs', $pluginsurl.'/flat-ui/js/application.js');
		wp_enqueue_script('applicationjs');
		
		wp_register_script('image-picker.js', $pluginsurl.'/image-picker/image-picker.js');
		wp_enqueue_script('image-picker.js');
		
		wp_register_style('image-picker.css', $pluginsurl.'/image-picker/image-picker.css');
		wp_enqueue_style('image-picker.css');
		
		
		
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'my-script-handle', plugins_url('admin_script.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
		
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
		wp_enqueue_media();
	}
	else
	{
		wp_enqueue_script('jquery');
	}
}
add_action('admin_enqueue_scripts', 'wccp_pro_enqueue_scripts');

//---------------------------------------------------------------------------------------------
//Add the plugin icon to the top admin bar
//---------------------------------------------------------------------------------------------
function wpccp_add_items($admin_bar)
{
	$pluginsurl = plugins_url( '', __FILE__ );
	global $post;
	$perma_link = '';
	$wccpadminurl = get_admin_url();
	//if($post->ID) $perma_link = get_permalink($post->ID);
	//if($_REQUEST['tag_ID']) $perma_link = get_category_link($_REQUEST['tag_ID']);
	
	//The properties of the new item. Read More about the missing 'parent' parameter below

    $args = array(
            'id'    => 'Protection',
            'title' => __('<img src="'.$pluginsurl.'/images/adminbaricon.png" style="vertical-align:middle;margin-right:5px;width: 22px;" alt="Protection" title="Protection" />Protection' ),
            'href'  => $wccpadminurl.'options-general.php?page=wccp-options-pro',
            'meta'  => array('title' => __('WP Content Copy Protection'),)
            );

	$sub_args_exclude = array(
            'id'    => 'WPCCPExclude',
			'parent' => 'Protection',
            'title' => __('Exclude this from protection' ),
            'href'  => "#",
            'meta'  => array('title' => __('WP Content Copy Protection'))
            );
	
	$sub_args_include = array(
            'id'    => 'WPCCP_Protect',
			'parent' => 'Protection',
            'title' => __('Protect this' ),
            'href'  => "#",
            'meta'  => array('title' => __('WP Content Copy Protection'))
            );
    $admin_bar->add_menu($args);
	
	global $wccp_pro_settings;
	if (get_blog_option_single_and_multisite('wccp_pro_settings')) $wccp_pro_settings = get_blog_option_single_and_multisite('wccp_pro_settings'); //To get refreshed array for ajax use

	$url_exclude_list = $wccp_pro_settings["url_exclude_list"];
	
	global $current_screen;
	//echo 'zzzzzzzzzzzzzzzzzzzz ' . $current_screen->id;
	//$cs = $current_screen->id;
	
	/*
	$targets = array('post', 'edit-category', '_category', '_tag');
	$match_by_type = false;
	foreach($targets as $t)
	{
		if (strpos($cs,$t) !== false) {
			$match_by_type = true;
			break;
		}
	}
	if(is_admin())
	{
		//if ($cs == "post" || $cs == "edit-category" || $cs == "_category" || $cs == "_tag")
		if($match_by_type)
		{
			if (strpos($url_exclude_list, $perma_link) !== false)
			{
				$admin_bar->add_menu($sub_args_include);
			}
			else
			{
				$admin_bar->add_menu($sub_args_exclude);
			}
		}
	}
	*/
}

if($c -> wccp_pro_get_setting('show_admin_bar_icon') == 'Yes')
{
	add_action('admin_bar_menu', 'wpccp_add_items',  40);
}

//---------------------------------------------------------------------------------------------
//Show settings page
//---------------------------------------------------------------------------------------------
function wccp_pro_options_page_pro()
{
	if( is_plugin_active( 'wp-content-copy-protector/preventer-index.php' ) )
	{
		echo '<p align="center" dir="ltr">&nbsp;</p>
				<p align="center" dir="ltr">&nbsp;</p>
				<p align="center" dir="ltr">&nbsp;</p>
				<p align="center" dir="ltr"><font size="5" color="#FF0000">Alert!</font></p>
				<p align="center" dir="ltr"><font size="5">The free version of WP Content Copy 
				Protection is still active</font></p>
				<p align="center" dir="ltr"><font size="5">Please deactivate it to start using the pro version</font></p>';
	}
	else
	{
		include 'admin_settings.php';
	}
}

//---------------------------------------------------------------------------------------------
//Make our function to call the WordPress function to add to the correct menu
//---------------------------------------------------------------------------------------------
function wccp_pro_add_options() {

	add_options_page('WP Content Copy Protection (pro)', 'WP Content Copy Protection (pro)', 'manage_options', 'wccp-options-pro', 'wccp_pro_options_page_pro');
}
add_action('admin_menu', 'wccp_pro_add_options');
?>