<?php ob_start();
/*
Plugin Name: WP Content Copy Protection & No Right Click (premium)

Plugin URI: http://www.wp-buy.com/

Description: This wp plugin protect the posts content from being copied by any other web site author , you dont want your content to spread without your permission!!

Version: 8.7

Author: wp-buy

Text Domain: wccp_pro_translation_slug

Domain Path: /languages

Author URI: http://www.wp-buy.com/
*/
/////Version number changed at 2/4/2016/////

//---------------------------------------------------------------------------------------------
//The updater
//---------------------------------------------------------------------------------------------
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://www.wp-buy.com/wp-update-server/?action=get_metadata&slug=wccp-pro',
	__FILE__, //Full path to the main plugin file or functions.php.
	'wccp-pro'
);

//---------------------------------------------------------------------------------------------
//Load plugin textdomain to load translations
//---------------------------------------------------------------------------------------------
function wccp_load_textdomain() {
  load_plugin_textdomain( 'wccp_pro_translation_slug', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' ); 
}
add_action( 'init', 'wccp_load_textdomain' );
//---------------------------------------------------------------------------------------------
//Report any error during activation
//---------------------------------------------------------------------------------------------
register_activation_hook( __FILE__, 'wccp_pro_my_activation_func' ); function wccp_pro_my_activation_func() {
    file_put_contents(__DIR__.'/my_loggg.txt', ob_get_contents());
}

//---------------------------------------------------------------------------------------------
//define all variables the needed alot
//---------------------------------------------------------------------------------------------
$wpccp_pluginsurl = plugins_url( '', __FILE__ );

include "common-functions.php";
$wccp_pro_settings = wccp_pro_read_options();
include "controls-functions.php";
include "private-functions.php";
include "js_functions.php";
include "css_functions.php";
include "play_functions.php";

$dw_query = '';

function wccp_pro_add_htaccess($insertion) {
    $htaccess_file = ABSPATH.'.htaccess';
	$filename = $htaccess_file;
	if (is_writable($filename)) {
		wccp_pro_insert_with_markers_htaccess($htaccess_file, 'wccp_pro_image_protection', '');//This will always clear the old watermarking rules
		return wccp_pro_insert_with_markers_htaccess($htaccess_file, 'wccp_pro_image_protection', (array) $insertion);
	} else {
		echo 'Watermarking code not saved!! The .htaccess file is not writable.';
	}
}
//---------------------------------------------------------------------------------------------
//Register libraries using new wordpress register_script & enqueue_script functions
//---------------------------------------------------------------------------------------------
function wccp_pro_modify_htaccess() {
	
	$wccp_pro_settings = wccp_pro_read_options();
	$pluginsurl = plugins_url( '', __FILE__ );
	$url = site_url();
	$url = wccp_pro_get_domain($url);
	$hotlinking_rule_text = 'RewriteRule ^.*$ - [NC,L]';
	$mysite_rule_text = 'RewriteRule ^.*$ - [NC,L]';
	
	$type = 'dw';
	$dw_position = $wccp_pro_settings['dw_position'];
	$dw_text = $wccp_pro_settings['dw_text'];
	$dw_r_text = $wccp_pro_settings['dw_r_text'];
	$dw_font_color = $wccp_pro_settings['dw_font_color'];
	$dw_r_font_color = $wccp_pro_settings['dw_r_font_color'];
	$dw_font_size_factor = $wccp_pro_settings['dw_font_size_factor'];
	$dw_r_font_size_factor = $wccp_pro_settings['dw_r_font_size_factor'];
	$dw_text_transparency = $wccp_pro_settings['dw_text_transparency'];
	$dw_rotation = $wccp_pro_settings['dw_rotation'];
	$dw_imagefilter = $wccp_pro_settings['dw_imagefilter'];
	$dw_signature = $wccp_pro_settings['dw_signature'];
	$dw_logo = $wccp_pro_settings['dw_logo'];
	$dw_margin_left_factor = $wccp_pro_settings['dw_margin_left_factor'];
	$dw_margin_top_factor = $wccp_pro_settings['dw_margin_top_factor'];
	$watermark_caching = $wccp_pro_settings['watermark_caching'];
	$upload_dir = wp_upload_dir();
	$basedir = $upload_dir['basedir'];  //   /home3/server-folder/sitefoldername.com/wp-content/uploads
	$home_path = get_home_path();
	
	$exclude_online_services = trim($wccp_pro_settings['exclude_online_services']);
	$exclude_online_services = wccp_pro_multiexplode(array("," , "\r\n", "\n", "\r", "|"),$exclude_online_services);
	$exclude_online_services = esc_attr(implode("|", $exclude_online_services));
	$exclude_online_services_rule_text = 'RewriteCond %{HTTP_USER_AGENT} !(' . $exclude_online_services . ')  [NC]' . "\n	";
	$exclude_online_services_rule_text .= 'RewriteCond %{HTTP_REFERER} !^http(s)?://(www\.)?(' . $exclude_online_services . ')  [NC]';
	
	$excluded_images_from_watermarking = trim($wccp_pro_settings['excluded_images_from_watermarking']);
	$excluded_images_from_watermarking = wccp_pro_multiexplode(array("," , "\r\n", "\n", "\r", "|"),$excluded_images_from_watermarking);
	$excluded_images_from_watermarking = esc_attr(implode("|", $excluded_images_from_watermarking));
	$excluded_images_from_watermarking = str_replace(".", "\.", $excluded_images_from_watermarking);
	if($excluded_images_from_watermarking == '') $excluded_images_from_watermarking = "this_is_just_not_any_wanted_image_name";
	$excluded_images_from_watermarking_rule_text = 'RewriteCond %{REQUEST_URI} (' . $excluded_images_from_watermarking . ')  [NC,OR]' . "\n	";
	
	$exclude_registered_images_sizes = $wccp_pro_settings['exclude_registered_images_sizes'];
	$exclude_registered_images_sizes = wccp_pro_multiexplode(array("," , "\r\n", "\n", "\r", "|"),$exclude_registered_images_sizes);
	$exclude_registered_images_sizes = esc_attr(implode("|", $exclude_registered_images_sizes));
	$exclude_registered_images_sizes = str_replace(".", "\.", $exclude_registered_images_sizes);
	if($exclude_registered_images_sizes == '') $exclude_registered_images_sizes = "this_is_just_not_any_wanted_image_size";
	$excluded_images_from_watermarking_rule_text .= 'RewriteCond %{REQUEST_URI} (' . $exclude_registered_images_sizes . ')  [NC]';
	
	$file_content = '<?php' . "\n";
	$file_content .= '$watermark_caching = "' .$watermark_caching. '";' . "\n";
	$file_content .= '$watermark_type = "' .$type. '";' . "\n";
	$file_content .= '$watermark_position = "' .$dw_position. '";' . "\n";
	$file_content .= '$watermark_r_text = "' .$dw_r_text. '";' . "\n";
	$file_content .= '$r_font_size_factor = "' .$dw_r_font_size_factor. '";' . "\n";
	$file_content .= '$watermark_text = "' .$dw_text. '";' . "\n";
	$file_content .= '$font_size_factor = "' .$dw_font_size_factor. '";' . "\n";
	$file_content .= '$pure_watermark_stamp_image = "' .$dw_logo. '";' . "\n";
	
	$file_content .= '$margin_left_factor = "' .$dw_margin_left_factor. '";' . "\n";
	$file_content .= '$margin_top_factor = "' .$dw_margin_top_factor. '";' . "\n";
	$file_content .= '$watermark_color = "' .$dw_font_color. '";' . "\n";
	$file_content .= '$watermark_r_color = "' .$dw_r_font_color. '";' . "\n";
	$file_content .= '$watermark_transparency = "' .$dw_text_transparency. '";' . "\n";
	$file_content .= '$watermark_rotation = "' .$dw_rotation. '";' . "\n";
	$file_content .= '$watermark_imagefilter = "' .$dw_imagefilter. '";' . "\n";
	$file_content .= '$watermark_signature = "' .$dw_signature. '";' . "\n";
	$file_content .= '$home_path = "' .$home_path. '";' . "\n";
	$file_content .= '$upload_dir = "' .$basedir. '";' . "\n";
	$file_content .= '?>';
	
	$plugin_dir_path = plugin_dir_path( __FILE__ );
	$file = $plugin_dir_path . 'watermarking-parameters.php';  // (Can write to this file)
	
	// Write the contents back to the file
	@file_put_contents($file, $file_content);
	
	$dw_query = "type=dw&position=$dw_position&text=$dw_text&font_color=$dw_font_color&r_text=$dw_r_text&r_font_color=$dw_r_font_color&font_size_factor=$dw_font_size_factor&r_font_size_factor=$dw_r_font_size_factor&text_transparency=$dw_text_transparency&rotation=$dw_rotation&imagefilter=$dw_imagefilter&signature=$dw_signature&stamp=$dw_logo&margin_left_factor=$dw_margin_left_factor&margin_top_factor=$dw_margin_top_factor&home_path=$home_path";
	$dw_query = '';
	$hotlinking_rule = $wccp_pro_settings['hotlinking_rule'];
	if($hotlinking_rule == "Watermark"){
		$hotlinking_rule_text = 'RewriteRule ^(.*)\.(jpg|png|jpeg)$ ' . $pluginsurl . '/watermark.php?'. $dw_query . '&src=/$1.$2' . '&w=1' . ' [R=301,NC,L]';
	}else if ($hotlinking_rule == "No Action"){
		$hotlinking_rule_text = 'RewriteRule ^.*$ - [NC,L]';
	}
	
	$mysite_rule = $wccp_pro_settings['mysite_rule'];
	if($mysite_rule == "Watermark"){
		$mysite_rule_text = 'RewriteRule ^(.*)\.(jpg|png|jpeg)$ ' . $pluginsurl . '/watermark.php?'. $dw_query . '&src=/$1.$2' . '&w=1' . ' [R=301,NC,L]';
	}
	else
	{
		$mysite_rule_text = 'RewriteRule ^.*$ - [NC,L]';
	}
	
	$prevented_agents_rule_text = 'RewriteRule ^.*$ '. $pluginsurl . '/watermark.php [R=301,L]';
	
	$ruls[] = <<<EOT
	<IfModule mod_rewrite.c>
	RewriteEngine on
EOT;
	
	$ruls[] = <<<EOT
	RewriteCond %{HTTP_USER_AGENT} (PrintFriendly.com)
	$prevented_agents_rule_text
	
	RewriteCond %{HTTP_COOKIE} (WccpProCookie=excludethispage) [NC]
	RewriteRule ^.*$ - [NC,L]
	
	RewriteCond %{QUERY_STRING} (wccp_pro_watermark_pass) [NC,OR]
	RewriteCond %{REQUEST_URI} (wp-content/plugins) [NC,OR]
	RewriteCond %{REQUEST_URI} (wp-content/themes) [NC,OR]
	$excluded_images_from_watermarking_rule_text
	RewriteRule ^.*$ - [NC,L]
	
	# What happen to images on my site
	#RewriteCond %{HTTP_ACCEPT} (image|png) [NC]
	RewriteCond %{HTTP_REFERER} ^http(s)?://(www\.)?$url [NC,OR]
	RewriteCond %{HTTP_REFERER} ^(.*)$url [NC]
	$mysite_rule_text
	
	#Save as or Click on View image after right click or without any referer
	RewriteCond %{REQUEST_URI} (wpcphotclicked) [NC,OR]
	RewriteCond %{REQUEST_URI} (stackpathcdn.com) [NC,OR]
	RewriteCond %{HTTP_USER_AGENT} (stackpathcdn.com) [NC,OR]
	RewriteCond %{HTTP_ACCEPT} (text|html|application|image|png) [NC]
	$hotlinking_rule_text
	
	RewriteCond %{REQUEST_URI} \.(jpg|jpeg|png)$ [NC]
	RewriteCond %{REMOTE_ADDR} !^(127.0.0.1|162.144.5.62)$ [NC]
	RewriteCond %{REMOTE_ADDR} !^66.6.(32|33|36|44|45|46|40). [NC]
	$exclude_online_services_rule_text
	RewriteCond %{HTTP_REFERER} !^http(s)?://(www\.)?(www.$url|$url|pinterest.com|tumblr.com|facebook.com|plus.google|twitter.com|googleapis.com|googleusercontent.com|ytimg.com|gstatic.com) [NC]
	RewriteCond %{HTTP_USER_AGENT} !(linkedin.com|googlebot|msnbot|baiduspider|slurp|webcrawler|teoma|photon|facebookexternalhit|facebookplatform|pinterest|feedfetcher|ggpht) [NC]
	RewriteCond %{HTTP_USER_AGENT} !(photon|smush.it|akamai|cloudfront|netdna|bitgravity|maxcdn|edgecast|limelight|tineye) [NC]
	RewriteCond %{HTTP_USER_AGENT} !(developers|gstatic|googleapis|googleusercontent|google|ytimg) [NC]
	$hotlinking_rule_text
	
</ifModule>
EOT;
//NC (no case, case insensitive, useless in this context) and L (last rule if applied)
	wccp_pro_add_htaccess($ruls);
}
register_activation_hook( __FILE__, 'wccp_pro_modify_htaccess' );
add_action( 'upgrader_process_complete', 'wccp_pro_modify_htaccess',10, 2);

//---------------------------------------------------------------------------------------------
//Create a watermarked images directory within the Uploads Folder when plugin activated
//---------------------------------------------------------------------------------------------
function create_watermarked_images_directory() {
    $can_cache = false;
	$upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/wccp_pro_watermarked_images';
    if (! is_dir($upload_dir)) {
       $can_cache = mkdir( $upload_dir, 0755 );
    }
	return $can_cache;
}
register_activation_hook( __FILE__, 'create_watermarked_images_directory' );
//---------------------------------------------------------------------------------------------
//Register libraries using new wordpress register_script & enqueue_script functions
//---------------------------------------------------------------------------------------------
function wccp_pro_clear_htaccess()
{
	$htaccess_file = ABSPATH.'.htaccess';
	
	wccp_pro_insert_with_markers_htaccess($htaccess_file, 'wccp_pro_image_protection', "");
}
register_deactivation_hook( __FILE__, 'wccp_pro_clear_htaccess' );

function wccp_pro_insert_with_markers_htaccess( $filename, $marker, $insertion ) {
    if (!file_exists( $filename ) || is_writeable( $filename ) ) {
        if (!file_exists( $filename ) ) {
            $markerdata = '';
        } else {
            $markerdata = explode( "\n", implode( '', file( $filename ) ) );
        }
 
        if ( !$f = @fopen( $filename, 'w' ) )
            return false;
 
        $foundit = false;
        if ( $markerdata ) {
            $state = true;
            foreach ( $markerdata as $n => $markerline ) {
                if (strpos($markerline, '# BEGIN ' . $marker) !== false)
                    $state = false;
                if ( $state ) {
                    if ( $n + 1 < count( $markerdata ) )
                        fwrite( $f, "{$markerline}\n" );
                    else
                        fwrite( $f, "{$markerline}" );
                }
                if (strpos($markerline, '# END ' . $marker) !== false) {
                    fwrite( $f, "# BEGIN {$marker}\n" );
                    if ( is_array( $insertion ))
                        foreach ( $insertion as $insertline )
                            fwrite( $f, "{$insertline}\n" );
                    fwrite( $f, "# END {$marker}\n" );
                    $state = true;
                    $foundit = true;
                }
            }
        }
        if (!$foundit) {
            fwrite( $f, "\n# BEGIN {$marker}\n" );
			if ( is_array( $insertion ))
				foreach ( $insertion as $insertline )
					fwrite( $f, "{$insertline}\n" );
            fwrite( $f, "# END {$marker}\n" );
        }
        fclose( $f );
        return true;
    } else {
        return false;
    }
}
//---------------------------------------------------------------------
//To use debug console in PHP because its just allowed using JavaScript 
//---------------------------------------------------------------------
function wccp_pro_debug_to_console($data)
{
	global $wccp_pro_settings;
	 
	if(array_key_exists("developer_mode", $wccp_pro_settings))
	{	
		if($wccp_pro_settings['developer_mode'] == "Yes")
		{
			$output = $data;
			if ( is_array( $output ))
			{
				foreach ( $output as $element )
					echo "<script>console.log('Debug Objects: " . $element . "' );</script>";
			}
		}
	}
}
//---------------------------------------------------------------------------------------------
//Register libraries using new wordpress register_script & enqueue_script functions
//---------------------------------------------------------------------------------------------
function wccp_pro_get_domain($url)
{
	$nowww = preg_replace('/www\./','',$url);
	
	$domain = parse_url($nowww);
	
	preg_match("/[^\.\/]+\.[^\.\/]+$/", $nowww, $matches);
	
	if(count($matches) > 0)
	{
		return $matches[0];
	}
	else
	{
		return FALSE;
	}
}

//---------------------------------------------------------------------------------------------
//Returns true if $search_for is a substring of $search_in
//---------------------------------------------------------------------------------------------
function wccp_pro_contains($search_in, $search_for)
{
    return strpos($search_in, $search_for) !== false;
}

function inStr($needle, $haystack)
{
  $needlechars = strlen($needle); //gets the number of characters in our needle
  $i = 0;
  for($i=0; $i < strlen($haystack); $i++) //creates a loop for the number of characters in our haystack
  {
    if(substr($haystack, $i, $needlechars) == $needle) //checks to see if the needle is in this segment of the haystack
    {
      return TRUE; //if it is return true
    }
  }
  return FALSE; //if not, return false
}
//---------------------------------------------------------------------------------------------
//Register Main plugin Actions and Filters
//http://www.mysite.com/mypage.html
//http://www.mysite.com/myfolder/*
//---------------------------------------------------------------------------------------------
$self_url = wccp_pro_get_self_url();

$exclude_this_page = 'False';

$tag = '';

$url_exclude_list = $c -> wccp_pro_get_setting('url_exclude_list');

// Processes \r\n's first so they aren't converted twice.
$url_exclude_list = str_replace("\\n", "\n", $url_exclude_list);

$self_url = trim($self_url);

$self_url = preg_replace('{/$}', '', $self_url);

$urlParts = parse_url($self_url);

if(isset($urlParts['scheme'])) $urlParts_scheme = $urlParts['scheme'] . '://'; else $urlParts_scheme = '';

if(isset($urlParts['host'])) $urlParts_host = $urlParts['host']; else $urlParts_host = '';

if(isset($urlParts['path'])) $urlParts_path = $urlParts['path']; else $urlParts_path = '';

if(isset($urlParts['query'])) $urlParts_query = '?' . $urlParts['query']; else $urlParts_query = '';

$self_url = $urlParts_scheme . $urlParts_host . $urlParts_path . $urlParts_query;

//echo $self_url;

$url_exclude_list = wccp_pro_multiexplode(array("," ," ", "\n", "|"),$url_exclude_list);

wccp_pro_debug_to_console($url_exclude_list);

if( !empty($url_exclude_list) )
	{
		for ($i=0; $i <= count($url_exclude_list); $i++)
		{
			if (isset($url_exclude_list[$i]))
			{
				$tag = $url_exclude_list[$i];
				
				$tag = trim($tag);
				
				//$tag = rtrim($tag, "/");
				
				//echo '<br>' . $tag;
			}
			else
			{
				$tag = '';
			}
			if (wccp_pro_contains($tag, '/*')) //Bulk exclusion
			{
				$tag = str_replace("/*", "", $tag);
				
				if (wccp_pro_contains($self_url, $tag))
				{
					$exclude_this_page = 'True';
					
					break;
				}
			}
			else
			{
				if ($self_url == $tag || $self_url. '/' == $tag )
				{
					$exclude_this_page = 'True';
					
					break;
				}
			}
		}
	}

function wccp_pro_get_current_user_roles($mm = array()) {
	if ( ! function_exists( 'wp_get_current_user' ) )
	{
		require_once( ABSPATH . 'wp-includes/pluggable.php' );
	}
	if( is_user_logged_in() ) {
	$user = wp_get_current_user();
	$roles = ( array ) $user->roles;
	return $roles; // This returns an array
	} else {
	return array();
	}
}
$allowed_roles = array();

global $wccp_pro_settings;

if(!$wccp_pro_settings) $wccp_pro_settings = wccp_pro_read_options();

if (array_key_exists("exclude_by_user_type",$wccp_pro_settings))
{
	if(is_array($wccp_pro_settings['exclude_by_user_type']))
		$allowed_roles = $wccp_pro_settings['exclude_by_user_type'];
}

if(!defined('AUTH_COOKIE'))
	$roles = array();
else
	$roles = wccp_pro_get_current_user_roles();
	

if( array_intersect($roles, $allowed_roles) ) {
	$exclude_this_page = 'True';
}

setcookie("WccpProCookie", "", time() - 3600, "/"); // Clear the cookie
$wccp_pro_is_admin = false;
if ( is_admin()) $wccp_pro_is_admin = true;
if($exclude_this_page == 'True' || $wccp_pro_is_admin)
{
	// Set the expiration date to one hour ago
	$value = "excludethispage";
	$cookie_time = time()+15; // cookie time is small by default
	if($wccp_pro_is_admin) $cookie_time = time()+120; //increase cookie time for admin area
	setcookie("WccpProCookie", $value, $cookie_time , "/", "", false, true); //set a timed cookie
}

if($exclude_this_page != 'True')
{
	add_action('wp_head','wccp_pro_main_settings'); //Located on play_functions.php
	
	add_action('wp_head','wccp_pro_disable_hot_keys'); //Located on play_functions.php
	
	add_action('wp_footer','wccp_pro_disable_selection_settings_footer'); //Located on play_functions.php
	
	add_action('wp_head','wccp_pro_right_click_premium_settings'); //Located on
	
	add_action('wp_head','wccp_pro_css_settings'); //Located on
	
	add_action('wp_footer','wccp_pro_alert_message'); //Located on common-functions.php
	
	add_action('wp_footer','wccp_pro_global_js_scripts'); //Located on common-functions.php
	
	add_filter('body_class','wccp_pro_class_names'); //Located on play_functions.php
	
	add_action('wp_footer','wccp_pro_images_overlay_settings'); //Located on play_functions.php
	
	add_action('wp_footer','wccp_pro_videos_overlay_settings'); //Located on play_functions.php
	
	add_action('wp_head','wccp_pro_nojs_inject'); //Located on preventer-index.php
	
	add_filter( 'the_content', 'wccp_pro_find_image_urls'); //Located on common-functions.php
}

add_action( 'admin_footer', 'wccp_pro_alert_message' );

/*
function replace_text_222($text) {
	//$text = str_replace('jpg', 'jpg?x=wccp_pro_watermark_pass', $text);
	$replace = array(
        // 'WORD TO REPLACE' => 'REPLACE WORD WITH THIS'
        'jpg' => 'jpg?x=wccp_pro_watermark_pass',
    );
    $text = str_replace(array_keys($replace), $replace, $text);
	return $text;
}
add_filter('the_content', 'replace_text_222');
*/
//add_action( 'admin_footer', 'wccp_pro_cache_purge_action_js' );

function wccp_pro_cache_purge_action_js() { 
global $post;
if($post->ID) $my_permalink = get_permalink($post->ID);
if($_REQUEST['tag_ID']) $my_permalink = get_category_link($_REQUEST['tag_ID']);
?>
  <script type="text/javascript" >
     jQuery("li#wp-admin-bar-WPCCPExclude .ab-item").on( "click", function() {
        var data = {
                      'action': 'example_cache_purge',
					  'permalink': '<? echo $my_permalink; ?>',
                    };
		if(jQuery("li#wp-admin-bar-WPCCPExclude .ab-item").text() !== "Exclusion Done!")
		{
			jQuery("li#wp-admin-bar-WPCCPExclude .ab-item").text('Loading..');
			/* since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php */
			jQuery.post(ajaxurl, data, function(response) {
			   jQuery("li#wp-admin-bar-WPCCPExclude .ab-item").text('Exclusion Done!');
			});
		}
       
      });
  </script> <?php
}

/* Here you hook and define ajax handler function */

add_action( 'wp_ajax_example_cache_purge', 'wccp_pro_example_cache_purge_callback' );

function wccp_pro_example_cache_purge_callback() {
    /* You cache purge logic should go here. */
	$wccp_pro_settings = wccp_pro_read_options();
	$wccp_pro_settings["url_exclude_list"] = $wccp_pro_settings["url_exclude_list"] . "\n" . $_REQUEST['permalink'];
	update_blog_option_single_and_multisite( 'wccp_pro_settings' , $wccp_pro_settings );
    $response = $wccp_pro_settings["url_exclude_list"];
    echo ($response);
    wp_die(); /* this is required to terminate immediately and return a proper response */
} 

//---------------------------------------------------------------------------------------------
//Add plugin settings link to Plugins page
//---------------------------------------------------------------------------------------------
function wccp_pro_plugin_add_settings_link( $links ) {

	$settings_link = '<a href="options-general.php?page=wccp-options-pro">' . __( 'Settings' ) . '</a>';
	
	array_push( $links, $settings_link );
	
	return $links;
}

$plugin = plugin_basename( __FILE__ );

add_filter( "plugin_action_links_$plugin", 'wccp_pro_plugin_add_settings_link' );


//---------------------------------------------------------------------------------------------
//Function to get self url
//---------------------------------------------------------------------------------------------
function wccp_pro_get_self_url()
{ 
    return get_site_url().$_SERVER['REQUEST_URI'];
}

function wccp_pro_strleft($s1, $s2) { return substr($s1, 0, strpos($s1, $s2)); }

//---------------------------------------------------------------------------------------------
//Multiexplode function
//---------------------------------------------------------------------------------------------
function wccp_pro_multiexplode($delimiters,$string)
{   
	if(is_array($string))
		$ready = implode(",", $string); //Convert any array to comma_separated string
	else
		$ready = $string;
	$ready = str_replace(" ", "", $ready);
	$ready = str_replace($delimiters, $delimiters[0], $ready);//Replace all string delimiters with the first delimiter in the array
	$ready = str_replace($delimiters[0].$delimiters[0], $delimiters[0], $ready);
	$launch = explode($delimiters[0], $ready);
	return  $launch;
}

//---------------------------------------------------------------------------------------------
//Add nojs action
//---------------------------------------------------------------------------------------------
function wccp_pro_nojs_inject()
{
	global $wccp_pro_settings;
	if ($wccp_pro_settings['no_js_action'] == 'Watermark all')
	{
		if (!isset($_SESSION["no_js"]))
		{
			$pluginsurl = plugins_url( '', __FILE__ );
			
			$nojs_page_url = $pluginsurl . '/no-js.php';
			
			$referrer = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			
			$nojs_page_url = $nojs_page_url . "?referrer=" .$referrer;
			
			$st = "
				<!-- Redirect to another page (for no-js support) -->
				<noscript><meta http-equiv=\"refresh\" content=\"0;url=$nojs_page_url\"></noscript>
				
				<!-- Show a message -->
				<noscript>You dont have javascript enabled! Please download Google Chrome!</noscript>
			";

			echo $st;
		}
	}
}
if (isset($_SESSION["no_js"]))
{
	add_filter( 'the_content', 'wccp_pro_replace_image_urls');
}

//---------------------------------------------------------------------------------------------
//Replace image urls with nothing
//---------------------------------------------------------------------------------------------
function wccp_pro_replace_image_urls( $content ) {

	global $post;
	
	$wccp_pro_settings = wccp_pro_read_options();
	
	$dw_position = $wccp_pro_settings['dw_position'];
	$dw_text = $wccp_pro_settings['dw_text'];
		$dw_text = str_replace(" ","+",$dw_text);
	$dw_r_text = $wccp_pro_settings['dw_r_text'];
		$dw_r_text = str_replace(" ","+",$dw_r_text);
	$dw_font_color = $wccp_pro_settings['dw_font_color'];
	$dw_r_font_color = $wccp_pro_settings['dw_r_font_color'];
	$dw_font_size_factor = $wccp_pro_settings['dw_font_size_factor'];
	$dw_r_font_size_factor = $wccp_pro_settings['dw_r_font_size_factor'];
	$dw_text_transparency = $wccp_pro_settings['dw_text_transparency'];
	$dw_rotation = $wccp_pro_settings['dw_rotation'];
	$dw_imagefilter = $wccp_pro_settings['dw_imagefilter'];
	$dw_signature = $wccp_pro_settings['dw_signature'];
		$dw_signature = str_replace(" ","+",$dw_signature);
	$dw_logo = $wccp_pro_settings['dw_logo'];
	
	$dw_query = "type=dw&position=$dw_position&text=$dw_text&font_color=$dw_font_color&r_text=$dw_r_text&r_font_color=$dw_r_font_color&font_size_factor=$dw_font_size_factor&r_font_size_factor=$dw_r_font_size_factor&text_transparency=$dw_text_transparency&rotation=$dw_rotation&imagefilter=$dw_imagefilter&signature=$dw_signature&stamp=$dw_logo";
	
	$dw_query = str_replace("#","%23",$dw_query);
	
	$pluginsurl = plugins_url( '', __FILE__ );

	$regexp = '<img[^>]+src=(?:\"|\')\K(.[^">]+?)(?=\"|\')';

	//Watermark images inside the content
	if(preg_match_all("/$regexp/", $content, $matches, PREG_SET_ORDER))
	{
		if( !empty($matches) )
		{
			for ($i=0; $i <= count($matches); $i++)
			{
				if (isset($matches[$i]) && isset($matches[$i][0]))
				{
					$img_src = $matches[$i][0];
				}
				else
				{
					$img_src = '';
				}
				$url_parser = parse_url($img_src); //Array [scheme] => http    [host] => www.example.com    [path] => /foo/bar    [query] => hat=bowler&accessory=cane
				
				$img_file_path = $url_parser['path'];
				
				//$http = $pluginsurl . "/watermark.php?w=watermarksaveas.png&p=c&q=90&src=";
				
				$http = $pluginsurl . '/watermark.php?'. $dw_query . '&src=';

				$encrypted_img_src = $http . $img_file_path;

				$content = str_replace($img_src,$encrypted_img_src,$content);
			}
		}
	}
	$content = str_replace(']]>', ']]&gt;', $content);

return $content;
}

?>