=== WP Content Copy Protection & No Right Click (premium) ===
Contributors: wp-buy	
Tags: content, content copy protection, content protection, copy protection, prevent copy, protect blog, image protect, image protection, no right click, plagiarism, secure, theft
Requires at least: 3.5
Tested up to: 5.3.2
Stable tag: 8.7

This wp plugin protects the content of the posts from being copied by any other web site author, you don't want your content to spread without your permission!!


== Description ==
This wp plugin protects the content of the posts from being copied by any other web site author, you don't want your content to spread without your permission!!

**Improve your SEO score in Google and Yahoo and other SE's**:
Our plugin protects your content from being copied by any other web sites so your posts will still unique content, this is the best option for SEO

**Don't Let Your Stories Go to web thief!**
The plugin will keep your posts and home page protected by multiple techniques (JavaScript + CSS), this techniques does not found in any other WordPress plugin and you will own it for free with this plugin

**Easy to Install**:
Read the installation steps to find that this plugin does not need any coding or theme editing, just use your mouse.

**The Pro Edition Features include:**
1. Protect your content from selection and copy.
2. No one can right-click on images from your site if you want
3. Get full control on Right-click or context menu
4. Show alert messages, when the user made right click on images, text boxes, links, plain text.. etc
5. Disable the keys  CTRL+A, CTRL+C, CTRL+X, CTRL+S, or CTRL+V.
6. Advanced and easy to use control panel.    
7. Admin can exclude Home page Or Single posts from being copy protected
8. Admin can disable copy protection for admin users.
9. Aggressive image protection (it's not easy or it's impossible for expert users to steal your images !!)
10. Compatible with all major theme frameworks
11. Compatible with all major browsers


== Screenshots ==
1. WP Content Copy Protection premium admin page

== Installation Guide ==
**Installation steps**
1.Download the package.
2.Extract the contents of WP-Content-Copy-Protection.zip to wp-content/plugins/ folder  You should get a folder called WP-Content-Copy-Protection
3.Activate the Plugin in WP-Admin.
4.Goto Settings > **WP-Content-Copy-Protection** to configure options.
5.You will find **4 options** to protect your content,images,homepage and css protection. dont forget to **save** the changes before exit
Thanks!


== Changelog ==
=8.7=
<ul>
<li>Videos protection is now stronger than any other plugin</li>
<li>New feature, For the first time! watermarked is not out of exclusion anymore. Any excluded page will be excluded from image watermarking too</li>
<li>New feature, Wordpress media library is now excluded from image watermarking</li>
<li>New feature, Posts & Pages editor are also excluded from image watermarking</li>
<li>Fix bug for CTRL+(p,s,u) with editable text boxes</li>
<li>Fix bug with image smart protection CSS</li>
<li>New feature, Exclude registered images sizes from watermarking</li>
<li>New feature, Exclude images by name & size (manually) from watermarking</li>
<li>New feature, to enable/disable drag & drop. This feature is needed for LMS ,quizing and e-learning systems</li>
<li>Selection Exclude by class name bug fix</li>
<li>Exclude online services bug fix</li>
<li>Exclude by user type feature improved to list all user types</li>
<li>Disable special keys function improvment</li>
<li>All editable text inputs are now allowed to copy/paste</li>
<li>Selection issue fixed for SmartPhones</li>
<li>Update to the new bootstrap version</li>
<li>Use RTL-bootstrap version for RTL languages</li>
</ul>
=8.5=
<ul>
<li>Admin panel new theme, responsive & light</li>
<li>Drag and drop function is now separated as an option</li>
<li>Video protection is now better and improved</li>
<li>Full screen video protection still not supported, still trying to work around this issue</li>
<li>Exclude by user type function improved to get all user types</li>
<li>Improvments to be more compatible with Learning Managmant Systems LMS</li>
</ul>
=8.4=
<ul>
<li>Fix Exclusion by class name settings code</li>
</ul>
=8.3=
<ul>
<li>Fix laguages & translation issues</li>
</ul>
=8.2=
<ul>
<li>Remove a hidden field , It was used to make print screen protection function better but now we don't need it</li>
<li>Fix the function that remember which tab is last clicked</li>
<li>Fix jQuery errors and replace all old ($) with function (jQuery)</li>
<li>New button to clear cache for watermarked images</li>
</ul>
=8.1=
<ul>
<li>separate between selection functions and hotkey functions</li>
<li>New option to stop CTRL + A</li>
<li>New option to stop CTRL + C</li>
<li>New option to stop CTRL + V</li>
<li>New option to stop CTRL + S</li>
<li>New option to stop CTRL + X</li>
<li>New option to stop CTRL + U</li>
<li>New option to stop CTRL + P</li>
<li>New option to stop F12</li>
<li>Update on option Ctrl + P</li>
<li>Fix for inputs and any content editable filed selection exclusion</li>
<li>Check with the last wodpress version 5.3.2</li>
</ul>
=7.2=
<ul>
<li>Class exclusion issue fixed</li>
</ul>
=7.1=
<ul>
<li>Watermark caching added</li>
</ul>
=6.9.2=
<ul>
<li>Improve CDN watermarking exclusion</li>
</ul>

=6.9.1=
<ul>
<li>Improve Selection Exclude by class name</li>
</ul>

=6.9=
<ul>
<li>Sanitizing: Cleaning User Input data</li>
<li>Escaping: Securing Output all output data</li>
<li>Multisite installation supported now</li>
<li>Fix error generated during activation on some websites</li>
<li>Remove the option to exclude admin from protection from custom settings tab</li>
<li>Include new 5 options to exclude from protection by user type</li>
<li>Adding a checking message to know if The htaccess file is writable or not</li>
<li>Adding a function to report any error during activation</li>
</ul>

=6.8.2=
<ul>
<li>Fix function name duplication</li>
</ul>

=6.8.1=
<ul>
<li>Fix undefined index issue shows with 6.8 update</li>
<li>Now, Your browser will still remember the last opened tab inside the plugin admin panel</li>
</ul>

=6.8=
<ul>
<li>New feature added to exclude users by type from protection, you can find this feature options under the exclusion tab</li>
</ul>

=6.7=
<ul>
<li>Fix preview alert mesage button inside admin panel</li>
</ul>

=6.6=
<ul>
<li>Fix error with cutom CSS settings</li>
</ul>
=6.5=
<ul>
<li>Fix a big with apostrophe as it was replace every single qutation with '/ and if you go again in settings it will be '\\\\ etc..., this is fixed now</li>
<li>Check compatapility with wordpress 5.2.2</li>
</ul>

=6.4=
<ul>
<li>Improve Smart protection techniques using new jQuery functions</li>
<li>Fix to show mouse pointer as a hand pointer over links</li>
</ul>

=6.3.1=
<ul>
<li>Fix issue with mac os CMD button and right click</li>
<li>Protect your pages from the chrome extension called (Allow Select And Copy)</li>
<li>Protect your pages from the chrome extension called (Simple Allow Copy)</li>
</ul>

=6.2.3=
<ul>
<li>DropDown menu CSS fix</li>
</ul>

=6.2.2=
<ul>
<li>Fix error (undefined mesage) showin after update</li>
</ul>

=6.2.1=
<ul>
<li>fix php-update checker</li>
<li>Change old style with new one</li>
<li>Update interface to be 100% responsive</li>
<li>Replace JavaScript tabs with pure CSS tabs</li>
<li>Enhance online bots exclusion</li>
<li>PHP7 compatibility fix</li>
<li>Watermark find website url function fix</li>
</ul>

=5.0.0.1=
Bug fix
some style fixes
Fix watermark issues

==4.0.0.6==
Fix exclude urls problem

==4.0.0.5==
Fix alert when using hotkeys with textboxes

==4.0.0.4==
Fix confliction when activate the plugin without deactivating the free version

==4.0.0.2==
Fix watermarking home_path null value
Fix transparent image absulute url and make it relative

==4.0.0.1==
Add disable pritscreen option "works good with some browsers
Add option to enable or disable Ctrl+P option
Add option to enable or disable Ctrl+S option
Allow Message Show Time to be have zero value

=3.0.0.13=
Bug fix

=3.0.0.11=
Bug fix

=3.0.0.10=
Make watermarking image url without shown parameters

=3.0.0.9=
Prevent print screen with firefox

=3.0.0.8=
Fix transparent images watermarking problem

=3.0.0.7=
Fix Division by zero in error inside watermark.php file

=3.0.0.6=
Fix the scrollbar issue with safari

=3.0.0.4=
Fix the relative images path problem
Fix allow_url_fopen which is blocked by some servers

=3.0.0.3=
Admin can disable copy protection for logged in/registered users
disable the possible shortcut keys for copying the Text
You can also choose where this Plugin should work like All Pages (including Home Page and all other Pages & Posts) or Home Page or Custom Pages/Posts using the Settings Page options.
Multiple Text and Image Protection methods

=3.0.0.2=
Advanced Image Protection using Responsive Lightbox
Protect your Text and Images by Disabling the Mouse Right Click and Possible Shortcut Keys for Cut (CTRL+x), Copy (CTRL+c), Paste (CTRL+v), Select All(CTRL+a), View Source (CTRL+u) etc.

=3.0.0.1=
control the protection to be on users only (if admin here dont protect)
Option to Display Alert Message on Mouse Right Click.
Enable Right Click on Hyperlink Option Added
Right click problem fixed on static pages
New flat interface

=2.0.0.4=
<ul>
<li>Compatible with the new 4.2.1 version</li>
<li>Add coloring settins to colorize the alert message</li>
<li>Add Restore defaults Button</li>
</ul>
= 2.0.0.3 =
<ul>
<li>Adding adminbar link and icon redirecting you to the plugin settings page</li>
<li>Adding settings link into the plugins list page</li>
</ul>
= 2.0.0.2 =
<ul>
<li>Adding isset() function to all variables</li>
<li>Improving alert message</li>
<li>Fixing CTRL + U issue</li>
<li>Fixing CSS tricks</li>
</ul>
= 1.5.0.1 =
<ul>
<li>Fixing error (Warning: join(): Invalid arguments passed in /home/retailmakeover/public_html/wp-includes/post-template.php on line 478)</li>
</ul>
= 2.0.0.1 =
<ul>
<li>Admin can disable copy protection for logged in/admin users</li>
<li>disable the possible shortcut keys for copying the Text</li>
<li>You can also choose where this Plugin should work like All Pages (including Home Page and all other Pages & Posts) or Home Page or Custom Pages/Posts using the Settings Page options.</li>
<li>Multiple Text and Image Protection methods</li>
<li>Advanced Image Protection using Responsive Lightbox</li>
<li>Protect your Text and Images by Disabling the Mouse Right Click and Possible Shortcut Keys for Cut (CTRL+x), Copy (CTRL+c), Paste (CTRL+v), Select All(CTRL+a), View Source (CTRL+u) etc.</li>
<li>control the protection to be on users only (if admin here dont protect)</li>
<li>Option to Display Alert Message on Mouse Right Click.</li>
<li>Enable Right Click on Hyperlink Option Added</li>
<li>Right click problem fixed on static pages</li>
<li>New flat interface</li>
</ul>
= 1.0 =
<ul>
<li>initial version</li>
<li>static pages bug fixed</li>
<li>home page problem fixed</li>
<li>Add new Style</li>
</ul>