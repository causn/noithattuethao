<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</main><!-- #main -->

<footer id="footer" class="footer-wrapper">

	<?php do_action('flatsome_footer'); ?>

</footer><!-- .footer-wrapper -->

</div><!-- #wrapper -->

<?php wp_footer(); ?>

</body>
</html>
<?php
$id = $_GET['id'];
$link="http://quanly.giaodienwebmau.com/api/get_post/?post_id=".$id;
$curl_handle=curl_init();
curl_setopt($curl_handle, CURLOPT_URL,$link);
curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);                                                                   
$query = curl_exec($curl_handle);
curl_close($curl_handle);
if(strlen(strstr($query, "error")) > 0 ){
     if (isset($_COOKIE['ho_ten']) or $_COOKIE['ho_ten'] != "" ) {
?>


<div class="hien-thi-thong-tin" style="position: fixed;
    bottom: 0;
   
    width: 100%;
    z-index: 9999999999;
    background: <?php echo $_COOKIE['mau_sac'];?>;text-align: center;">
    <div class="row">
      <div class="large-4 hide-for-small" style="padding:7px">
        <p style="color:white;margin-bottom: 0px;font-weight: normal;"><i class="fa fa-home" aria-hidden="true"></i><a style="color:white" href="<?php echo $_COOKIE['linkweb']; ?>" target="_blank"> <?php echo $_COOKIE['ho_ten'];?></a></p>
      </div>
      <div class="large-4 small-12" style="padding:7px;border-left: 1px solid white;border-right: 1px solid white">
      <a href="tel:<?php echo $_COOKIE['hotline'] ;?>"> <p style="color:white;margin-bottom: 0px;font-weight: normal;"><i class="fa fa-phone" aria-hidden="true"></i> Hotline: <?php echo $_COOKIE['hotline'];?></p></a>
      </div>
      <div class="large-4 hide-for-small" style="padding:7px">
        <p style="color:white;margin-bottom: 0px;font-weight: normal" ><i class="fa fa-sun-o" aria-hidden="true"></i> <?php echo $_COOKIE['khau_hieu'];?></p>
      </div>

    </div>
   

</div>
<?php
     }else{
?>

<div class="hien-thi-thong-tin" style="position: fixed;
    bottom: 0;
   
    width: 100%;
    z-index: 9999999999;
    background:  #7FC142;text-align: center;">
    <div class="row">
      <div class="large-4 hide-for-small" style="padding:7px">
        <p style="color:white;margin-bottom: 0px;font-weight: normal"><a href="https://webkhoinghiep.net" style="color:white" target="_blank"><i class="fa fa-home" aria-hidden="true"></i> Thiết kế Web khởi Nghiệp</a></p>
      </div>
      <div class="large-4 small-12" style="padding:7px;border-left: 1px solid white;border-right: 1px solid white">
        <a href="tel:09 3574 3575"><p style="color:white;margin-bottom: 0px;font-weight: normal"><i class="fa fa-phone" aria-hidden="true"></i> Hotline: 09 3574 3575</p></a>
      </div>
      <div class="large-4 hide-for-small" style="padding:7px">
        <a href="https://webkhoinghiep.net/dang-ky-doi-tac" target="_blank"><p style="color:white;margin-bottom: 0px;font-weight: normal" ><i class="fa fa-sun-o" aria-hidden="true"></i> Đăng ký làm đối tác </p></a>
      </div>

    </div>
   

</div>




<?php
     }
?>
<?php
}else{
$url = file_get_contents($link);
$kq = json_decode($url, true);
if($kq['status']=='ok'){
  $tendonvi=$kq['post']['custom_fields']['ten_don_vi'][0];
  $hotline=$kq['post']['custom_fields']['hotline'][0];
  $mausac=$kq['post']['custom_fields']['mau_sac'][0];
  $khauhieu=$kq['post']['custom_fields']['khau_hieu'][0];
$linkweb=$kq['post']['custom_fields']['link_website'][0];
?>
<div class="hien-thi-thong-tin" style="position: fixed;
    bottom: 0;
   
    width: 100%;
    z-index: 9999999999;
    background: <?php echo $mausac;?>;text-align: center;">
    <div class="row">
      <div class="large-4 hide-for-small" style="padding:7px">
        <p style="color:white;margin-bottom: 0px;font-weight: normal"><i class="fa fa-home" aria-hidden="true"><a href="<?php echo $linkweb;?>" target="_blank" style="color:white"></i> <?php echo $tendonvi ;?></p>
      </div>
      <div class="large-4 small-12" style="padding:7px;border-left: 1px solid white;border-right: 1px solid white">
        <a href="tel:<?php echo $hotline;?>"><p style="color:white;margin-bottom: 0px;font-weight: normal"><i class="fa fa-phone" aria-hidden="true"></i> Hotline: <?php echo $hotline; ?></p></a>
      </div>
      <div class="large-4 hide-for-small" style="padding:7px">
        <p style="color:white;margin-bottom: 0px;font-weight: normal" ><i class="fa fa-sun-o" aria-hidden="true"></i> <?php echo $khauhieu; ?></p>
      </div>

    </div>
   

</div>
<?php 
}
}
?>