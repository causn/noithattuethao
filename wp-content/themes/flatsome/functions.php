<?php
/**
 * Flatsome functions and definitions
 *
 * @package flatsome
 */

require get_template_directory() . '/inc/init.php';
add_filter('woocommerce_empty_price_html', 'custom_call_for_price'); 
function custom_call_for_price() { return '<span class="lien-he-price">Liên hệ</span>'; }
define( 'WP_DEBUG', true );
/**
 * Note: It's not recommended to add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * Learn more here: http://codex.wordpress.org/Child_Themes
 */
